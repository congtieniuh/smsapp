package com.example.tien.broadcast.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import com.example.tien.broadcast.entities.Message;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tien on 9/26/2017.
 */

public class DBProcessing extends SQLiteOpenHelper {
    private static  final String DB_NAME = "MessageDB";
    private static  final int VERSION = 1;

    public DBProcessing(Context context){
        super(context,DB_NAME,null,VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("create table message (id INTEGER PRIMARY KEY autoincrement , fromAddress TEXT ,content TEXT)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
    public void onPersist(Message m){
        Log.i("HCT", "onPersist: insert thanh cong");
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();
        ContentValues values = new ContentValues();
        values.put("fromAddress",m.getFromAdress());
        values.put("content",m.getContent());

        sqLiteDatabase.insert("message",null,values);

    }
    public String getName(String phone){
        Cursor cursor = getReadableDatabase().rawQuery("select name from contact where phoneNumber = "+phone+" ",null);
        String res = "";
        if(cursor != null && cursor.moveToFirst()){
            do{
                res = cursor.getString(cursor.getColumnIndex("name"));
            }while (cursor.moveToNext());
        }
        return res;
    }
    public List<Message> getListMessage(){
        Cursor cursor = getReadableDatabase().query("message",new String[]{"fromAddress","content"},null,null,"fromAddress",null,null);
        List<Message> messageList = new ArrayList<>();
        if(cursor != null && cursor.moveToFirst()){
            do {
                String fromAddress = cursor.getString(cursor.getColumnIndex("fromAddress"));
                String content = cursor.getString(cursor.getColumnIndex("content"));
                Message message = new Message(fromAddress,content);
                messageList.add(message);
            }while (cursor.moveToNext());
        }
        return messageList;
    }
}
