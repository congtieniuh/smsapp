package com.example.tien.broadcast.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.tien.broadcast.abstraction.ItemClickRecycler;
import com.example.tien.broadcast.R;
import com.example.tien.broadcast.entities.Message;

import java.util.List;

/**
 * Created by tien on 9/16/2017.
 */

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MessageHolder>{
    List<Message> messageList= null;
    RecyclerView recyclerView;
    Context context;

    ItemClickRecycler itemClick;

    public MessageAdapter(List<Message> messageList, RecyclerView   recyclerView, Context context, ItemClickRecycler itemClick) {
        this.messageList = messageList;
        this.recyclerView = recyclerView;
        this.context = context;

        this.itemClick = itemClick;
    }

    public MessageAdapter(List<Message> messageList, RecyclerView recyclerView) {
        this.messageList = messageList;
        this.recyclerView = recyclerView;
    }

    @Override
    public MessageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_message,parent,false);
        final MessageHolder holder = new MessageHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClick.onClick(view,holder.getAdapterPosition());
            }
        });
        return  holder;
    }

    @Override
    public void onBindViewHolder(MessageHolder holder, int position) {
        if(position == 0){
            Animation animation = AnimationUtils.loadAnimation(context,android.R.anim.slide_in_left);
            holder.itemView.startAnimation(animation);

        }
        Message message = messageList.get(position);

        holder.tvFrom.setText(message.getFromAdress());
        holder.tvContent.setText(message.getContent());
        holder.img.setImageBitmap(message.getImage());

        holder.tvFrom.setTextColor(Color.BLUE);

    }


    @Override
    public int getItemCount() {
        return messageList.size();
    }
    static class MessageHolder extends RecyclerView.ViewHolder{
        TextView tvFrom,tvContent;
        ImageView img;
        public MessageHolder(View itemView) {
            super(itemView);
            tvFrom = itemView.findViewById(R.id.tv_from);
            tvContent = itemView.findViewById(R.id.tv_content_message);
            img = itemView.findViewById(R.id.row_message_img);
        }

    }
}
