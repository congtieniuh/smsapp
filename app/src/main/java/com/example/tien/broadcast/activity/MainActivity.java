package com.example.tien.broadcast.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.tien.broadcast.R;
import com.example.tien.broadcast.dao.DBProcessing;
import com.example.tien.broadcast.entities.Message;

public class MainActivity extends AppCompatActivity {
    TextView tv;
    private int MY_PERMISSIONS_REQUEST = 0x58;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tv = (TextView) findViewById(R.id.tv);
//        String permission = Manifest.permission.RECEIVE_SMS;
//        String readContact_permission = Manifest.permission.READ_CONTACTS;
//        String writeContact_permission = Manifest.permission.WRITE_CONTACTS;
//        int grant = ContextCompat.checkSelfPermission(this, permission);
//        if ( grant != PackageManager.PERMISSION_GRANTED) {
//            String[] permission_list = new String[10];
//            permission_list[0] = permission;
//            permission_list[1] = readContact_permission;
//            permission_list[2] = writeContact_permission;
//            ActivityCompat.requestPermissions(this, permission_list, 1);
//        }
        if (checkSelfPermission(Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS,Manifest.permission.RECEIVE_SMS,Manifest.permission.WRITE_CONTACTS},
                    MY_PERMISSIONS_REQUEST);
            // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
            // app-defined int constant

            return;
        }
        DBProcessing db = new DBProcessing(this);



    }


    public void sendNotify(View view) {
        Intent intent = new Intent(this,ContactsAndMessageActivity.class);
        startActivity(intent);



    }
}
