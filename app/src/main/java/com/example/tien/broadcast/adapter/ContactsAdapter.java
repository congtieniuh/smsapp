package com.example.tien.broadcast.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.tien.broadcast.R;
import com.example.tien.broadcast.abstraction.ItemClickRecycler;
import com.example.tien.broadcast.dao.ReadContacts;
import com.example.tien.broadcast.entities.Contacts;

import java.util.List;

/**
 * Created by tien on 10/3/2017.
 */
public  class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.ContactHolder>{
    List<Contacts> list_contacts;
    Context context;
    ItemClickRecycler itemClick;

    public ContactsAdapter(List<Contacts> list_contacts, Context context, ItemClickRecycler itemClick) {
        this.list_contacts = list_contacts;
        this.context = context;
        this.itemClick = itemClick;
    }

    @Override
    public ContactHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_contacts,parent,false);
        final ContactHolder holder = new ContactHolder(rootView);
        rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClick.onClick(view,holder.getAdapterPosition());
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(ContactHolder holder, int position) {
        Contacts contacts = list_contacts.get(position);
        holder.phoneName.setText(contacts.getPhoneName());

        ReadContacts readContacts = new ReadContacts();

        holder.imageView.setImageBitmap(contacts.getImage());
    }


    @Override
    public int getItemCount() {
        return list_contacts.size();
    }
    static class ContactHolder extends RecyclerView.ViewHolder{
        ImageView imageView;
        TextView phoneName;
        public ContactHolder(View itemView) {
            super(itemView);
            phoneName = itemView.findViewById(R.id.row_contacts_phoneName);
            imageView = itemView.findViewById(R.id.row_contact_img);
        }
    }
}