package com.example.tien.broadcast.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.example.tien.broadcast.R;
import com.example.tien.broadcast.adapter.MyPgAdapter;
import com.example.tien.broadcast.dao.DBProcessing;
import com.example.tien.broadcast.dao.ReadContacts;
import com.example.tien.broadcast.entities.Message;
import com.example.tien.broadcast.fragment.ContactsFragment;
import com.example.tien.broadcast.fragment.MessageFragment;

import java.util.ArrayList;
import java.util.List;

public class ContactsAndMessageActivity extends AppCompatActivity {
    private FragmentTabHost mTabHost;
    private ViewPager pager;
    private TabLayout tabLayout;
    private String body = "";
    private String address = "";
    private MessageFragment messageFragment = new MessageFragment();
    private ContactsFragment contactsFragment = new ContactsFragment();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts_and_message);

        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS)!= PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this,Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED
                ){
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.RECEIVE_SMS,Manifest.permission.READ_CONTACTS
            ,Manifest.permission.CALL_PHONE,Manifest.permission.SEND_SMS},0x0);

        }
        ViewPager viewPager= (ViewPager) findViewById(R.id.vpViewPager);
        tabLayout = (TabLayout) findViewById(R.id.tlTabLayout);
        //tạo tab
        tabLayout.addTab(tabLayout.newTab().setText("MESSAGE"));
        tabLayout.addTab(tabLayout.newTab().setText("CONTACT"));

        List<Fragment> fragments = getFragments();

        MyPgAdapter adapter = new MyPgAdapter(getSupportFragmentManager(),fragments);
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        Log.i("HCT", "ContactID: "+new ReadContacts().getAllContactsID(this));

        DBProcessing dbProcessing = new DBProcessing(this);
        dbProcessing.onPersist(new Message("asd","sads"));
    }

    private List<Fragment> getFragments() {
        List<Fragment> fragments = new ArrayList<>();
        messageFragment = MessageFragment.newInstance("MESSAGE");
        contactsFragment = ContactsFragment.newInstance("CONTACT");
        fragments.add(messageFragment);
        fragments.add(contactsFragment);
        return fragments;
    }



    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
/*        Bundle b = intent.getExtras();

         body = b.getString("body");
         address = b.getString("address");

        Bundle dataBundle = new Bundle();
        dataBundle.putString("address",address);
        dataBundle.putString("body",body);
        MessageFragment messageFragment = new MessageFragment();
        messageFragment.setArguments(dataBundle);
        getSupportFragmentManager().beginTransaction().replace(R.id.containerMessage,messageFragment).commit();*/

        Bundle bdlInfo = intent.getBundleExtra("messageInfo");
        messageFragment.onSaveInstanceState(bdlInfo);

    }

}
