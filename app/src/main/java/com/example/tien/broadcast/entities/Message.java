package com.example.tien.broadcast.entities;

import android.graphics.Bitmap;

/**
 * Created by tien on 9/16/2017.
 */

public class Message {
    private Bitmap image;
    private String fromAdress;
    private String content;
    private String name;

    public Message(Bitmap image, String fromAdress, String content, String name) {
        this.image = image;
        this.fromAdress = fromAdress;
        this.content = content;
        this.name = name;
    }

    public Message(Bitmap image, String fromAdress, String content) {
        this.image = image;
        this.fromAdress = fromAdress;
        this.content = content;
    }

    public Message(String fromAdress, String content) {
        this.fromAdress = fromAdress;
        this.content = content;

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Message message = (Message) o;

        return fromAdress.equals(message.fromAdress);
    }

    @Override
    public int hashCode() {
        return fromAdress.hashCode();
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public String getFromAdress() {
        return fromAdress;
    }

    public void setFromAdress(String fromAdress) {
        this.fromAdress = fromAdress;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
