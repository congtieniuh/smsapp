package com.example.tien.broadcast.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tien.broadcast.R;

public class MessageActivity extends AppCompatActivity {
    TextView content,from;
    String numberDdestination= "";
    EditText edtInput;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);
        content = (TextView) findViewById(R.id.ms_content);
        from = (TextView) findViewById(R.id.ms_from);
        edtInput = (EditText) findViewById(R.id.edt_input_sms);
        Intent intent = getIntent();
        if(intent!=null){
            String content_ms = intent.getStringExtra("content");
            String from_address = intent.getStringExtra("from");
            content.setText(content_ms);
            from.setText(from_address);
        }else{
            Toast.makeText(this, "Null", Toast.LENGTH_SHORT).show();
        }

        numberDdestination = intent.getStringExtra("PHONE_NUMBER");


    }

    public void SendMessage(View view) {
        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(numberDdestination,null,edtInput.getText().toString(),null,null);
        edtInput.setText("");
    }
}
