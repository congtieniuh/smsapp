package com.example.tien.broadcast.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.example.tien.broadcast.abstraction.ItemClickRecycler;
import com.example.tien.broadcast.adapter.MessageAdapter;
import com.example.tien.broadcast.R;
import com.example.tien.broadcast.dao.DBProcessing;
import com.example.tien.broadcast.dao.ReadContacts;
import com.example.tien.broadcast.entities.Message;

import java.util.ArrayList;
import java.util.List;

public class ListMessage extends AppCompatActivity implements ItemClickRecycler {
    private RecyclerView recyclerView;
    private List<Message> messageList;
    private String body = "";
    private String address = "";
    private MessageAdapter adapter = null;
    private ImageView img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_message);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        img = (ImageView) findViewById(R.id.row_message_img);

        messageList = new ArrayList<>();
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new MessageAdapter(messageList,recyclerView,this,this);
        recyclerView.setAdapter(adapter);

        receiveFirstMessage();


    }

    @Override
    protected void onNewIntent(Intent intent) {

        Log.i("HCT", "start ----->");
        Bundle b = intent.getExtras();

        body = b.getString("body");
        address = b.getString("address");

        DBProcessing db = new DBProcessing(this);
        // String name = db.getName(address);
        addItem();
    }

    private void receiveFirstMessage(){
        Intent intent = getIntent();
        Bundle b = intent.getExtras();

        body = b.getString("body");
        address = b.getString("address");


        // String name = db.getName(address);
        addItem();


    }
    private void addItem(){
        ReadContacts myContact = new ReadContacts();
        DBProcessing db = new DBProcessing(this);
        //get photo from phone number
        Bitmap bitmap = myContact.openPhoto(myContact.getContactIDFromNumber(address,this),this);

        String ctName = myContact.getContactName(address,this);

        if (ctName.equals("")) {
            Message message = new Message(bitmap, address, body);

            if (messageList.contains(message)) {
                int pos = messageList.indexOf(message);
                Log.i("HCT", "onNewIntent: " + pos);
                messageList.remove(pos);
                messageList.add(0, message);

            } else {
                messageList.add(0, message);
            }
            db.onPersist(message);
        } else {
            Message message = new Message(bitmap, ctName, body);
            if (messageList.contains(message)) {
                int pos = messageList.indexOf(message);
                Log.i("HCT", "onNewIntent: " + pos);
                messageList.remove(pos);
                messageList.add(0, message);
            } else {
                messageList.add(0, message);
            }
            db.onPersist(message);
        }
        adapter.notifyDataSetChanged();
    }
    @Override
    public void onClick(View v, int pos) {


        Intent message_Intent = new Intent(this, MessageActivity.class);
        message_Intent.putExtra("content",messageList.get(pos).getContent());
        message_Intent.putExtra("from",messageList.get(pos).getFromAdress());
        startActivity(message_Intent);
    }

    public void moveToContacts(View view) {
        Intent intent = new Intent(this,ContactsActivity.class);
        startActivity(intent);
    }
}
