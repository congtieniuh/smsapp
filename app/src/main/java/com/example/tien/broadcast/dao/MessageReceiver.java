package com.example.tien.broadcast.dao;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

import com.example.tien.broadcast.activity.ContactsAndMessageActivity;
import com.example.tien.broadcast.activity.ListMessage;

/**
 * Created by tien on 9/16/2017.
 */

public class MessageReceiver extends BroadcastReceiver {
    public static final String ACTION = "android.provider.Telephony.SMS_RECEIVED";
    private String testGit = "testgit";
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent iListMsg = new Intent(context,ContactsAndMessageActivity.class);
        if(intent.getAction().equals(ACTION)){
            Bundle bundle = intent.getExtras();
            if(bundle!=null){
                Object[] pdusObj = (Object[]) bundle.get("pdus");
                for (int i = 0; i < pdusObj.length; i++) {
                    SmsMessage smg = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                    Bundle bInfo = new Bundle();
                    bInfo.putString("body",smg.getDisplayMessageBody());
                    bInfo.putString("address",smg.getDisplayOriginatingAddress());
                    iListMsg.putExtra("messageInfo",bInfo);
                }
            }
        }
        iListMsg.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(iListMsg);
    }
}

