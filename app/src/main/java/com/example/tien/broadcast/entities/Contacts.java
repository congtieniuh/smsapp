package com.example.tien.broadcast.entities;

import android.graphics.Bitmap;

/**
 * Created by tien on 9/29/2017.
 */

public class Contacts {
    private Bitmap image;
    private String phoneName;
   private String phoneNumber;


    public Contacts(Bitmap image, String phoneName, String phoneNumber) {
        this.image = image;
        this.phoneName = phoneName;
        this.phoneNumber = phoneNumber;
    }

    public Contacts(String phoneName, String phoneNumber) {
        this.phoneName = phoneName;
        this.phoneNumber = phoneNumber;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public String getPhoneName() {
        return phoneName;
    }

    public void setPhoneName(String phoneName) {
        this.phoneName = phoneName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Contacts contacts = (Contacts) o;

        return phoneNumber != null ? phoneNumber.equals(contacts.phoneNumber) : contacts.phoneNumber == null;

    }

    @Override
    public int hashCode() {
        return phoneNumber != null ? phoneNumber.hashCode() : 0;
    }
}
