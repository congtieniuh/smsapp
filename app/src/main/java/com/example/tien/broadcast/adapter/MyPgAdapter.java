package com.example.tien.broadcast.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

/**
 * Created by tien on 10/3/2017.
 */

public class MyPgAdapter extends FragmentPagerAdapter {
    List<Fragment> fragments;
    public MyPgAdapter(FragmentManager fm, List<Fragment> fragments) {
        super(fm);
        this.fragments=fragments;
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0: return "Sms";
            case 1: return "Contact";
            default:return "";
        }

    }
}

