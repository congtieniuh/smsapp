package com.example.tien.broadcast.fragment;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.tien.broadcast.CustomDialog;
import com.example.tien.broadcast.R;
import com.example.tien.broadcast.abstraction.ItemClickRecycler;
import com.example.tien.broadcast.activity.MessageActivity;
import com.example.tien.broadcast.adapter.ContactsAdapter;
import com.example.tien.broadcast.dao.ReadContacts;
import com.example.tien.broadcast.entities.Contacts;

import java.util.List;

/**
 * Created by tien on 10/1/2017.
 */

public class ContactsFragment extends Fragment implements ItemClickRecycler{
    private RecyclerView recyclerView_Contacts;
    private List<Contacts> listContact;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_contacts,container,false);
    }
    public static ContactsFragment newInstance(String message){
        ContactsFragment f = new ContactsFragment();
        Bundle bdl = new Bundle();
        bdl.putString("MESS",message);
        f.setArguments(bdl);
        return f;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView_Contacts = view.findViewById(R.id.recycler_listContacts);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView_Contacts.setLayoutManager(layoutManager);


//        List<Bitmap> bitmap = new ReadContacts().getPhotoContacts(getActivity());
//        for (int  i = 0 ;i<listContact.size();++i){
//            Log.i("HCT", "onViewCreated: " + listContact.get(i).getPhoneName());
//            listContact.get(i).setImage(bitmap.get(i));
//        }

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                listContact = new ReadContacts().getAllContact(getActivity());
                new ReadContacts().getAllPhoto(getContext(),listContact);
            }
        });

        ContactsAdapter adapter = new ContactsAdapter(listContact,getContext(),this);
        recyclerView_Contacts.setAdapter(adapter);
    }

    @Override
    public void onClick(View v, final int pos) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = getActivity().getLayoutInflater();
         View vDialog=  inflater.inflate(R.layout.dialog_contacts,null,false);
        builder.setView(vDialog);

        Button btnCall = vDialog.findViewById(R.id.btnCall);
        Button btnSms = vDialog.findViewById(R.id.btnSMS);

        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentCall = new Intent();
                intentCall.setAction(Intent.ACTION_CALL);
                intentCall.setData(Uri.parse("tel:"+listContact.get(pos).getPhoneNumber()));
                getContext().startActivity(intentCall);
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();

        btnSms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentSms = new Intent(getContext(), MessageActivity.class);
                String desNum = listContact.get(pos).getPhoneNumber();
                intentSms.putExtra("PHONE_NUMBER",desNum);
                startActivity(intentSms);
            }
        });

    }
}
