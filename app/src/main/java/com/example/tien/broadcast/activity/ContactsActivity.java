package com.example.tien.broadcast.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.tien.broadcast.R;
import com.example.tien.broadcast.abstraction.ItemClickRecycler;
import com.example.tien.broadcast.adapter.ContactsAdapter;
import com.example.tien.broadcast.dao.ReadContacts;
import com.example.tien.broadcast.entities.Contacts;

import java.util.List;

public class ContactsActivity extends AppCompatActivity implements ItemClickRecycler{
    private RecyclerView recyclerView_Contacts;

    private List<Contacts> listContact;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);
        recyclerView_Contacts = (RecyclerView) findViewById(R.id.recycler_listContacts);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView_Contacts.setLayoutManager(layoutManager);
        initData();
        ContactsAdapter adapter = new ContactsAdapter(listContact,this,this);
        recyclerView_Contacts.setAdapter(adapter);
    }
    private void initData(){
        listContact = new ReadContacts().getAllContact(this);
    }


    @Override
    public void onClick(View v, int pos) {

    }
}
