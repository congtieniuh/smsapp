package com.example.tien.broadcast;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;

/**
 * Created by tien on 10/10/2017.
 */

public class CustomDialog extends Dialog implements View.OnClickListener {
    Button btnCall;
    Button btnSms;
    public CustomDialog(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_contacts);
        btnCall = findViewById(R.id.btnCall);
        btnSms = findViewById(R.id.btnSMS);
        btnSms.setOnClickListener(this);
        btnCall.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view.equals(btnCall)){
            Intent intentCall = new Intent();
            intentCall.setAction(Intent.ACTION_CALL);
            intentCall.setData(Uri.parse("tel:"+012345));
            getContext().startActivity(intentCall);
        }
        if(view.equals(btnSms)){

        }
    }
}
