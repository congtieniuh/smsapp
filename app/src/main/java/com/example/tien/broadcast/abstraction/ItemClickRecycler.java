package com.example.tien.broadcast.abstraction;

import android.view.View;

/**
 * Created by tien on 9/23/2017.
 */

public interface ItemClickRecycler {
    void onClick(View v,int pos);
}
