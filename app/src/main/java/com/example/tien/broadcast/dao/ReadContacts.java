package com.example.tien.broadcast.dao;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.provider.ContactsContract;
import android.util.Log;
import android.widget.Toast;

import com.example.tien.broadcast.R;
import com.example.tien.broadcast.entities.Contacts;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by tien on 9/28/2017.
 */

public class ReadContacts {
    /**
     *  lay contacts name dua tren phone number
     * @param phoneNumber
     * @param context
     * @return contactName
     */
    public String getContactName(final String phoneNumber, Context context)
    {
        Uri uri=Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI,Uri.encode(phoneNumber));

        String[] projection = new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME};

        String contactName="";
        Cursor cursor=context.getContentResolver().query(uri,projection,null,null,null);

        if (cursor != null) {
            if(cursor.moveToFirst()) {
                contactName=cursor.getString(0);
            }
            cursor.close();
        }

        return contactName;
    }

    /**
     * Get photo contact
     * @param contactId
     * @param context
     * @return
     */
    public Bitmap openPhoto(long contactId, Context context) {

        // Uri truyền vào id danh bạ lấy từ hàm getContactIDFromNumber
        Uri contactUri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, contactId);
        Uri photoUri = Uri.withAppendedPath(contactUri, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);
        Cursor cursor = context.getContentResolver().query(photoUri,
                new String[] {ContactsContract.Contacts.Photo.PHOTO}, null, null, null);
        if (cursor == null) {
            return null;
        }
        try {

            if (cursor.moveToFirst()) {
                byte[] data = cursor.getBlob(0);
                if (data != null) {
                    return BitmapFactory.decodeStream(new ByteArrayInputStream(data));
                }
            }
        } finally {
            cursor.close();
        }
        return null;

    }
    @Deprecated
    /**
     * Ham nay bi loi
     */
    public List<Bitmap> getPhotoContacts(Context context){
        List<Bitmap> listContact = new ArrayList<>();
        Cursor cursor = context.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI,
                new String[]{ContactsContract.Contacts._ID},null,null,null);//lấy tuần tự id của tất cả contact
        if(cursor!= null && cursor.moveToFirst()){
            do{//duyệt mỗi lần lấy 1 photo trong danh bạ, từ trên xuống dưới
                Log.i("HCT", "test: "+cursor.getLong(0));
                Cursor cursor2 = context.getContentResolver().query(
                        ContactsContract.Data.CONTENT_URI,//from
                        new String[]{ContactsContract.Contacts.Photo.PHOTO},// select
                        ContactsContract.Contacts.Photo.CONTACT_ID+"=?",//where
                        new String[]{cursor.getLong(0)+""},//where args
                        null);
				
                //đã lấy được photo của CONTACT_ID đang duyệt
                if(cursor2!= null && cursor2.moveToFirst()){
                    do{
                        byte[] data = cursor2.getBlob(0);
                        if(data != null){//gán hình ảnh
                            listContact.add(BitmapFactory.decodeStream(new ByteArrayInputStream(data)));
                        }else{
                            listContact.add(BitmapFactory.decodeResource(context.getResources(),R.drawable.icuser));
                        }

                    }while(cursor2.moveToNext());
                    cursor2.close();
                }
            }while(cursor.moveToNext());
        }
        return listContact;
    }

    /**
     *  Lay tat ca cac hinh anh trong danh ba
     * @param context
     * @param listContact
     */
    public void getAllPhoto(Context context,List<Contacts> listContact){
        Cursor cursor = context.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI,
                new String[]{ContactsContract.Contacts._ID},null,null,null);//lấy tuần tự id của tất cả contact
        if(cursor!= null && cursor.moveToFirst()){
            int i = 0;
            do{//duyệt mỗi lần lấy 1 photo trong danh bạ, từ trên xuống dưới
                Cursor cursor2 = context.getContentResolver().query(
                        ContactsContract.Data.CONTENT_URI,//from
                        new String[]{ContactsContract.Contacts.Photo.PHOTO},// select
                        ContactsContract.Contacts.Photo.CONTACT_ID+"=?",//where
                        new String[]{cursor.getLong(0)+""},//where args
                        null);
                //đã lấy được photo của CONTACT_ID đang duyệt
                if(cursor2!= null && cursor2.moveToFirst()){
                    do{
                        byte[] data = cursor2.getBlob(0);
                        if(data != null){//gán hình ảnh
   //                         listContact.get(i).setImage(BitmapFactory.decodeStream(new ByteArrayInputStream(data)));
                        }else{//gán hình ảnh mặc định nếu không có hình (default: ic_launcher_round)
// \\                           listContact.get(i).setImage(BitmapFactory.decodeResource(context.getResources(),R.mipmap.ic_launcher_round));
                        }


                    }while(cursor2.moveToNext());
                    cursor2.close();
                }
                i++;//tăng i theo vị trí object Contact trong arrayList (Recycler View) để gán cho đúng chỗ
            }while(cursor.moveToNext());
        }
    }

    /**
     * Lay ra all contactId , co contactId moi co the lay duoc hinh
     * @param context
     * @return
     */
    public long getAllContactsID(Context context){
        String[] proj = new String[]
                {///as we need only this colum from a table...
                        ContactsContract.Contacts.DISPLAY_NAME,
                        ContactsContract.Contacts._ID,
                };
        long id= 0 ;
        String sortOrder1 = ContactsContract.CommonDataKinds.StructuredPostal.DISPLAY_NAME + " COLLATE LOCALIZED ASC";
        Cursor crsr = context.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI,proj, null, null, sortOrder1);
        while(crsr.moveToNext())
        {
            id = crsr.getLong(crsr.getColumnIndex(ContactsContract.Contacts._ID));
        }
        crsr.close();
        return id;
    }

    /**
     * get one Contact Id
     * @param contactNumber
     * @param context
     * @return phoneContactID
     */
    public  long getContactIDFromNumber(String contactNumber, Context context) {
        String UriContactNumber = Uri.encode(contactNumber);
        long phoneContactID = 0;
        Cursor contactLookupCursor = context.getContentResolver().query(Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, UriContactNumber),
                new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME, ContactsContract.PhoneLookup._ID}, null, null, null);
        while (contactLookupCursor.moveToNext()) {
            phoneContactID = contactLookupCursor.getLong(contactLookupCursor.getColumnIndexOrThrow(ContactsContract.PhoneLookup._ID));
        }
        contactLookupCursor.close();

        return phoneContactID;
    }

    /**
     *  lay ra tat ca cac contacts trong danh ba
     * @param context
     * @return listContacts
     */
    private final String DISPLAY_NAME = Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ?
            ContactsContract.Contacts.DISPLAY_NAME_PRIMARY : ContactsContract.Contacts.DISPLAY_NAME;
    public List<Contacts> getAllContact(Context context){
        List<Contacts> listContacts= new ArrayList<>();
        Cursor cur =  context.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        if (cur != null && cur.moveToFirst()) {
            do {
                String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));

                Integer hasPhone = cur.getInt(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
                if (hasPhone > 0) {
                    Cursor cp = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);
                    if (cp != null && cp.moveToFirst()) {
                        String phone = cp.getString(cp.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        String name =  cp.getString(cp.getColumnIndex(DISPLAY_NAME));
                        Contacts contacts = new Contacts(name,phone);
                        listContacts.add(contacts);
                        cp.close();
                    }
                }

            } while (cur.moveToNext());
        }
        return  listContacts;
    }

}
