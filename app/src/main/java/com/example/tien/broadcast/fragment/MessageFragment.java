package com.example.tien.broadcast.fragment;

import android.animation.ObjectAnimator;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.DecelerateInterpolator;
import android.widget.ProgressBar;

import com.example.tien.broadcast.R;
import com.example.tien.broadcast.abstraction.ItemClickRecycler;
import com.example.tien.broadcast.dao.DBProcessing;
import com.example.tien.broadcast.adapter.MessageAdapter;
import com.example.tien.broadcast.dao.ReadContacts;
import com.example.tien.broadcast.entities.Message;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * Created by tien on 10/1/2017.
 */

public class MessageFragment extends Fragment implements ItemClickRecycler{
    private RecyclerView recyclerView;
    private List<Message> messageList;
    private String body = "";
    private String address = "";
    private MessageAdapter adapter = null;
    private final String TAG ="HCT";
    ProgressBar mprogressBar;
    SwipeRefreshLayout refreshLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_message,container,false);

        return v;
    }
    public static MessageFragment newInstance(String message){
        MessageFragment f = new MessageFragment();
        Bundle bdl = new Bundle();
        bdl.putString("MESS",message);
        f.setArguments(bdl);
        return f;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView =  view.findViewById(R.id.recyclerMessage);
        mprogressBar = view.findViewById(R.id.circular_progress_bar);
        refreshLayout = view.findViewById(R.id.refreshLayout);

        //load progressbar
        mprogressBar.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.INVISIBLE);

        messageList = new ArrayList<>();
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new MessageAdapter(messageList,recyclerView,getActivity(),this);
        recyclerView.setAdapter(adapter);

        /**
         * Start load data from database
         */
        MessageTask task = new MessageTask();
        task.execute();


        refreshLayout.setRefreshing(false);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshLayout.setRefreshing(true);
                adapter.notifyDataSetChanged();
                refreshLayout.setRefreshing(false);
            }
        });

    }

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.i(TAG, "onSaveInstanceState: "+outState);
        if(outState != null && !outState.isEmpty()){
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    String address = outState.getString("address");
                    String body = outState.getString("body");

                    ReadContacts myContact = new ReadContacts();
                    DBProcessing db = new DBProcessing(getActivity());

                    /**
                     * get photo from phone number
                     */
                    Bitmap bitmap = myContact.openPhoto(myContact.getContactIDFromNumber(address,getActivity()),getActivity());

                    /**
                     * get Contacts Name
                     */
                    String ctName = myContact.getContactName(address,getActivity());

                    if (ctName.equals("")) {
                        Message message = new Message(bitmap, address, body);

                        if (messageList.contains(message)) {
                            int pos = messageList.indexOf(message);
                            Log.i("HCT", "onNewIntent: " + pos);
                            messageList.remove(pos);
                            messageList.add(0, message);

                        } else {
                            messageList.add(0, message);
                        }
                        db.onPersist(message);

                    } else {
                        Message message = new Message(bitmap, ctName, body);
                        if (messageList.contains(message)) {
                            int pos = messageList.indexOf(message);
                            Log.i("HCT", "onNewIntent: " + pos);
                            messageList.remove(pos);
                            messageList.add(0, message);
                        } else {
                            messageList.add(0, message);
                        }
                        db.onPersist(message);
                    }
                    adapter.notifyDataSetChanged();
                }
            });
        }

    }

    @Override
    public void onClick(View v, int pos) {

    }

    private class MessageTask extends AsyncTask<Void,String,Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            List<Message> listDB = new DBProcessing(getContext()).getListMessage();
            for (Message m:listDB){
                messageList.add(m);
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            recyclerView.setVisibility(View.VISIBLE);
            mprogressBar.setVisibility(View.GONE);
            adapter.notifyDataSetChanged();
            super.onPostExecute(aVoid);
        }
    }
}
